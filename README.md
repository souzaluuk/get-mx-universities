# get-mx-universities

Discovering who owns the Brazilian universities email servers

## About

This project was developed in order to get information about email servers from
Brazilian universities - specifically, which email providers the Brazilian
universities are using. Our main interest is about how much Brazilian
universities are using Google or Microsoft servers.

This script served as a tool to a paper published in [LAVITS'2019](http://www.lavits.ihac.ufba.br/).

## Requirements

* Linux
* Python 3
* [host](https://en.wikipedia.org/wiki/Host_(Unix)) command-line

## How to use

Put the email domains you wish to consult in a file, line-by-line. After it,
just run in a terminal:

`$ python3 get-mx-universities.py FILE`

where `FILE` is the filename you created for input. See [universities.txt](universities.txt)
to see an example about the file for input.

The script will print the information about email server accountable for each
domain and save it to a file named `output-YYYYMMDD-FILE`, where YYYYMMDD is
the year, month and day information when you ran the script.

## How it works

The script uses the `host` command to do a lookup for email servers information
related to a domain. The script captures only one line from the answer provided
by `host` command.

Follow, the script will look for a pattern in the server address related to the
email server. Different providers use some common address in its servers: for
example, Google uses 'google' or 'googlemail', Microsoft use 'outlook'.

The providers and the patterns utilized are described in providers variable, in
the [script](get-mx-universities.py).

## Extend

If you wish to extend the script in order to look for new providers or new
patterns in the target email server, see the [code](get-mx-universities.py) and
the instructions there.

Despite this project was created to look information from universities, in fact
it just run a massive email server consult to several domains. It is possible to
apply this script for different scenarios, not related to universities.

## License

This software is distributed under the [MIT license](LICENSE).
