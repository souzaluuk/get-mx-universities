#!/bin/bash

printf "\n\n## Relatório $(date '+%d-%m-%Y')" >> general-report.md

python get-mx-universities.py seducs.txt --db db/seducs.json --initial "S" --title "Secretarias de Educação"
python get-mx-universities.py federal-institutes.txt --db db/federal-institutes.json --initial "I" --title "Institutos Federais"
python get-mx-universities.py federal-universities.txt --db db/federal-universities.json --initial "F" --title "Universidades Federais"
python get-mx-universities.py state-universities.txt --db db/state-universities.json --initial "E" --title "Universidades Estaduais"